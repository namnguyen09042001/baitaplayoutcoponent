import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.css";

import Item from "./Item";
import Banner from "./Banner";
export default class Body extends Component {
  render() {
    return (
      <div>
        <Banner></Banner>
        <Item></Item>
      </div>
    );
  }
}
